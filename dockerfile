FROM ubuntu:focal

ENV TZ=Asia/Seoul

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
  && apt-get update -y \
  && apt-get install -y python3-pip python3-dev build-essential

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

ENTRYPOINT ["uvicorn", "main:app", "--reload","--host", "0.0.0.0", "--port", "80"]
